# librem one

- create an [Librem One](https://puri.sm/posts/the-new-librem-one-services/) account
- install Librem Chat and Librem Social
	- [ios](https://apps.apple.com/us/developer/purism-spc/id1448949014)
	- [android](https://play.google.com/store/apps/developer?id=Purism+SPC)

## social media
- [twitter](https://twitter.com/localnative_app) alternative

[https://social.librem.one/@yi](https://social.librem.one/@yi)

## more
[Librem 5](https://www.youtube.com/watch?v=4SwE9W8JasA)

[Arch Linux's list of applications](https://wiki.archlinux.org/index.php/list_of_applications)
