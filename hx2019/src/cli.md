#  command line interface - cli

[Debian](https://www.debian.org/) variants
```
sudo apt install vim emacs nano tree
```

mac [brew](https://brew.sh/)
```
brew install vim emacs nano tree
```


[awesome-shell](https://github.com/alebcay/awesome-shell)
