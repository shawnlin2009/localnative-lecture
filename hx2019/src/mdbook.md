# [mdbook](https://github.com/rust-lang-nursery/mdBook)
```
sudo apt install build-essential
cargo install mdbook
```
will take a while for many [crates](https://crates.io/) to be built the first time

```
cd ~/repos/localnative/localnative-lecture/hx2019
mdbook serve
mdbook build
```
# more
[other](https://github.com/myles/awesome-static-generators) [static website generator](https://www.staticgen.com/)

[Zola](https://github.com/getzola/zola)

[MkDocs](https://www.mkdocs.org/)

[Hugo](https://gohugo.io/)
