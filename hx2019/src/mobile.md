# mobile

[android](https://mozilla.github.io/firefox-browser-architecture/experiments/2017-09-21-rust-on-android.html)

[ios](https://mozilla.github.io/firefox-browser-architecture/experiments/2017-09-06-rust-on-ios.html)

## more
[rust-ios-android](https://github.com/kennytm/rust-ios-android#rust-ios-android)

[rust_on_mobile](https://github.com/Geal/rust_on_mobile)
