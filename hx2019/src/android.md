# android

## gnu/linux

install android targets
```
rustup target add x86_64-linux-android
```

content of `~/.cargo/config`
```
[target.x86_64-linux-android]
ar = "/home/USER/Android/Sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/x86_64-linux-android-ar"
linker = "/home/USER/Android/Sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/x86_64-linux-android28-clang"

[target.aarch64-linux-android]
ar = "/home/USER/Android/Sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/aarch64-linux-android-ar"
linker = "/home/USER/Android/Sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/aarch64-linux-android28-clang"

[target.armv7-linux-androideabi]
ar = "/home/USER/Android/Sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/arm-linux-androideabi-ar"
linker = "/home/USER/Android/Sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/armv7a-linux-androideabi28-clang"

[target.i686-linux-android]
ar = "/home/USER/Android/Sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/i686-linux-android-ar"
linker = "/home/USER/Android/Sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/i686-linux-android28-clang"
```

symbolic link
```
ln -s x86_64-linux-android28-clang x86_64-linux-android-clang
ln -s armv7a-linux-androideabi28-clang arm-linux-androideabi-clang
ln -s i686-linux-android28-clang i686-linux-android-clang
ln -s aarch64-linux-android28-clang aarch64-linux-android-clang
```

## more
[F-Droid](https://f-droid.org)
