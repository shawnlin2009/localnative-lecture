# rust

[rustup](https://rustup.rs/)
```
sudo apt install curl
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

`less ~/.profile`
```
export PATH="$HOME/.cargo/bin:$PATH"
```

run terminal as login shell

```
cargo install mdbook 
```
